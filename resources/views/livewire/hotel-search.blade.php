<div>
    @livewire('components.header')

    <div class="bg-gray-100">
        <div class="container mx-auto py-5">
            <form wire:submit.prevent="search" class="lg:flex   flex-col items-center content-center justify-center">
                <div class="mx-auto">
                    {{ $this->form }}
                </div>
                <div class="mx-auto content-center text-center mt-4">
                    <button type="submit" class="justify mr-auto  mx-auto
        border rounded px-5 py-2 bg-[#006ce4] text-white hover:bg-[#003b95]
        text-sm font-semibold shadow-sm ring-1 ring-insetx ring-gray-300">
                        Search
                    </button>
                </div>
            </form>
        </div>

    </div>
    <div class="flex flex-col container mx-auto mt-5 gap-2">
        {{-- TODO --}}

        @for($i=0;$i<=20;$i++)
            <div class="flex p-3 border rounded flex-wrap md:flex-row flex-col">
                <img class="w-40 h-40 object-cover rounded"
                     src="https://cf.bstatic.com/xdata/images/hotel/square200/47139711.webp?k=f29448a400561bf52bd02583e06509c1ebeee6f448df06a9d5000a3e09efd09b&o="
                     alt="">

                <div class="flex flex-col flex-1 bg-red-700s px-3 pt-1 ">
                    <a href=""
                       class="text-decoration-none font-bold hover:underline ">
                        Name of the hotel
                    </a>
                    <div class=" flex">


                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5">
                            <path fill-rule="evenodd"
                                  d="M9.69 18.933l.003.001C9.89 19.02 10 19 10 19s.11.02.308-.066l.002-.001.006-.003.018-.008a5.741 5.741 0 00.281-.14c.186-.096.446-.24.757-.433.62-.384 1.445-.966 2.274-1.765C15.302 14.988 17 12.493 17 9A7 7 0 103 9c0 3.492 1.698 5.988 3.355 7.584a13.731 13.731 0 002.273 1.765 11.842 11.842 0 00.976.544l.062.029.018.008.006.003zM10 11.25a2.25 2.25 0 100-4.5 2.25 2.25 0 000 4.5z"
                                  clip-rule="evenodd"/>
                        </svg>


                        <div class="">
                            Hotel Location in here yo ha
                        </div>


                    </div>
                    <div class="mt-2">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A asperiores assumenda atque beatae
                        blanditiis, corporis dicta, error, fugit impedit incidunt ipsam iste natus odit quasi quo
                        ratione sunt totam vitae?
                    </div>
                </div>

                <div class="flex flex-col mt-2 md:mt-0 ">
                    <div class="flex gap-1 justify-content-end mb-2 md:mb-0">
                        <div class="flex flex-col">
                            <div class="font-bold text-right">Review score</div>
                            <div class="text-xs text-right">3,581 Reviews</div>

                        </div>
                        <div class="w-10 h-10 bg-[#003b95]  text-center text-white rounded-md flex rounded-bl-sm">
                            <span class="my-auto mx-auto">5</span>
                        </div>

                    </div>
                    <div class="flex-1"></div>
                    <div class="flex flex-col text-left md:text-right">
                        <div class="text-sm">
                            Price from
                        </div>
                        <div class="price text-green-600 font-bold text-xl">$55,000</div>
                        <div class="text-xs">
                            Per night
                        </div>
                    </div>

                    {{--                    --}}
                    <a class=" text-center mt-2 rounded bg-[#006ce4]
                     text-white text-decoration-none cursor-pointer hover:bg-[#003b95]
                     py-2 text-sm font-bold">
                        Check availability
                    </a>
                </div>
            </div>
        @endfor
    </div>

</div>
