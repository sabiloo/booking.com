<div class="col-md-4">
    <div class="card tickets-price">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8"><b>Tickets (1 adult, 1 children)</b></div>
                <div class="col-md-4 text-right">{{ $price['fair'] }} €</div>
            </div>
            @if($price['baggage'] and $price['baggage'] != 0)
                <div class="row">
                    <div class="col-md-8">Baggage</div>
                    <div class="col-md-4 text-right">{{ $price['baggage'] }} €</div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-8">Taxes and charges</div>
                <div class="col-md-4 text-right">{{ $price['taxes'] }} €</div>
            </div>
            <div class="row total-price">
                <div class="col-md-8">Total</div>
                <div class="col-md-4 text-right">{{ $price['total'] }} €</div>
            </div>
        </div>
    </div>
</div>
