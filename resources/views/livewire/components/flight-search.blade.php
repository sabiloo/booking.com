<div class="">
    <div class="bg-gray-100">
        <div class="container mx-auto py-5">
            <form wire:submit.prevent="search" class="lg:flex   flex-col items-center content-center justify-center">
                <div class="mx-auto">
                    {{ $this->form }}
                </div>
                <div class="mx-auto content-center text-center mt-4">
                    <button type="submit" class="justify mr-auto  mx-auto
        border rounded px-5 py-2 bg-[#006ce4] text-white hover:bg-[#003b95]
        text-sm font-semibold shadow-sm ring-1 ring-insetx ring-gray-300">
                        Search
                    </button>
                </div>
            </form>
        </div>

    </div>
    <div class="flex flex-col container mx-auto mt-5 gap-2">
        {{-- TODO --}}

        @for($i=0;$i<=20;$i++)
            <livewire:components.flight-card-item :flight_id="$i + 1" />
        @endfor
    </div>
</div>
