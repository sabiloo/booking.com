<div class="bg-[#003B95] w-full">
    <div class="lg:flex lg:items-center lg:justify-between pt-6 pb-6 pr-6 pl-6 container mx-auto">
        <div class="min-w-0 flex-1">
            <h2 class="text-2xl font-bold leading-7 text-gray-900s
             sm:truncate sm:text-3xl sm:tracking-tight text-white">
                Booking.com
            </h2>

        </div>
        @if(!auth()->check())
            <div class="mt-5 flex lg:ml-4 lg:mt-0">
            <span class="hidden sm:block">
              <a href="{{ route('register') }}"
                 class="btn inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                 style="color: #006ec4; border-color: rgb(0, 108, 228);">
                Register
              </a>
            </span>

                <span class="ml-3 hidden sm:block">
              <a href="{{ route('filament.client.auth.login') }}"
                 class="btn inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                 style="color: #006ce4; border-color: rgb(0, 108, 228);">
                Sign In
              </a>
            </span>

                <!-- Dropdown -->
                <div class="relative ml-3 sm:hidden">
                    <button type="button"
                            class="inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:ring-gray-400"
                            id="mobile-menu-button" aria-expanded="false" aria-haspopup="true">
                        More
                        <svg class="-mr-1 ml-1.5 h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor"
                             aria-hidden="true">
                            <path fill-rule="evenodd"
                                  d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                                  clip-rule="evenodd"/>
                        </svg>
                    </button>
                    <div
                        class="absolute right-0 z-10 -mr-1 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                        role="menu" aria-orientation="vertical" aria-labelledby="mobile-menu-button" tabindex="-1">
                        <!-- Active: "bg-gray-100", Not Active: "" -->
                        <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1"
                           id="mobile-menu-item-0">Register</a>
                        <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1"
                           id="mobile-menu-item-1">Sign In</a>
                    </div>
                </div>
            </div>
        @else
            <div class="mt-5 flex lg:ml-4 lg:mt-0">
            <span class="hiddens sm:block">
              <a href="{{route('filament.client.pages.dashboard')}}"
                 class="btn inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                 style="color: #006ec4; border-color: rgb(0, 108, 228);">
                Enter to my profile
              </a>
            </span>

            </div>
        @endif
    </div>
    {{-- navigation-bar --}}
    <div class="  flex text-white container mx-auto  gap-1 pb-3">

        <a href="{{route('index')}}"
           class=" flex rounded-full py-2 px-3 cursor-pointer text-white gap-1
        hover:bg-[#1a4fa0] text-decoration-none
        {{request()->routeIs('index')?'border-white border bg-[#1a4fa0]':''}}">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                 stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round"
                      d="M6 12L3.269 3.126A59.768 59.768 0 0121.485 12 59.77 59.77 0 013.27 20.876L5.999 12zm0 0h7.5"/>
            </svg>

            <span>
                Flights
            </span>


        </a>

        <a href="{{route('hotel.index')}}"

           class=" flex  rounded-full py-2 px-3 cursor-pointer text-white gap-1
        hover:bg-[#1a4fa0] text-decoration-none
        {{request()->routeIs('hotel.*')?'border-white border bg-[#1a4fa0]':''}}">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                 stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round"
                      d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25"/>
            </svg>
            <span>
                Hotel
            </span>


        </a>
    </div>
    {{-- /navigation-bar --}}
</div>

