<div>

    <div class="w-full content-center text-center md:w-8/12 mx-auto shadow-smx ">
        <div class="flex border rounded-lg px-5x py-5x ">

            <div class="flex flex-1 px-4">
                {{-- Agency --}}
                <div class="my-auto text-center items-center justify-center flex flex-col ">
                    <img src="https://random.imagecdn.app/50/50" class="w-12 h-12 rounded justify-center"
                         alt="">
                    <span class="text-sm justify-center"> Agency </span>
                </div>
                {{-- /Agency --}}
                {{-- time --}}
                <div class="flex  mx-auto pt-2">
                    <div class="text-right mr-4">
                        <div class="font-bold text-lg">5:13
                            <span class="text-xs font-lightx ">PM</span></div>
                        <div class="">FAR</div>
                    </div>
                    <div class=" hidden sm:block">
                        <div class="">_______________</div>
                        <span class="mx-auto text-gray-400 text-sm">12h 24m</span>
                    </div>

                    <div class="text-left ml-4">
                        <div class="font-bold text-lg">5:13
                            <span class="text-xs font-lightx ">AM</span></div>
                        <div class="">FAR</div>
                    </div>
                </div>
                {{-- /time --}}

                {{-- separator --}}
                <div class="h-fullx bg-gray-200 w-[.5px]"></div>
                {{-- /separator --}}

                <div class="flex flex-col w-1/6 p-4x m-2">
                    <div class="text-right font-bold pr-3">$23</div>
                    <div class="text-left text-sm pl-3 pb-2">
                        Airport
                    </div>
                    <div wire:click="flight_detail" class="rounded bg-[#006ce4] hover:bg-[#003b95] mx-3
                         text-white shadow mb-2 py-0.5 cursor-pointer px-0.5">
                        View details
                    </div>
                </div>


            </div>

        </div>
    </div>

</div>
