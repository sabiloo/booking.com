<div>
    @livewire('components.header')
    <div class="container">
        <div class="row" style=" margin-top: 30px">
            <div class="col-md-4">
                <div style="width: 100%"><iframe width="100%" height="300px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street,%20Dublin,%20Ireland+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"><a href="https://www.maps.ie/population/">Find Population on Map</a></iframe></div>
            </div>
            <div class="col-md-8">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="/hotel.jpeg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/hotel.jpeg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/hotel.jpeg" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 30px; margin-bottom: 30px">
        <div class="row">
            <div class="col-md-4">
                <div class="card" style="padding: 10px">
                    <div class="card-body" style="font-size: 15px">
                        <span><i class="fa-solid fa-wifi" style="margin-right: 15px; font-size: 20px"></i></span>
                        Free Wifi
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="padding: 10px">
                    <div class="card-body" style="font-size: 15px">
                        <span><i class="fa-solid fa-kitchen-set" style="margin-right: 15px; font-size: 20px"></i></span>
                        Kitchen
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="padding: 10px">
                    <div class="card-body" style="font-size: 15px">
                        <span><i class="fa-solid fa-ban-smoking" style="margin-right: 15px; font-size: 20px"></i></span>
                        Non-smoking rooms
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card" style="padding: 10px">
                    <div class="card-body" style="font-size: 15px">
                        <span><i class="fa-solid fa-gem" style="margin-right: 15px; font-size: 40px"></i></span>
                        <span style="display: inline-block; font-size: 14px; font-weight: bold">
                            Not usually available – you're in luck!<br>
                        Charming apartment Center of Paris/Montorgueil ( Aboukir 78) is not usually available on our site. Reserve soon before it sells out!
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <p style="font-size: 14px; margin: 15px 0; line-height: 2.5">
                    Located 1.4 km from Pompidou Centre and 1.2 km from Louvre Museum, Charming apartment Center of Paris/Montorgueil ( Aboukir 78) offers accommodation in the centre of Paris. This apartment is 1.9 km from Notre Dame Cathedral and 1.7 km from Sainte-Chapelle.
                    <br>
                    With free WiFi, this 1-bedroom apartment provides a flat-screen TV, a washing machine and a kitchen with a microwave and toaster. Towels and bed linen are provided in the apartment.
                    <br>
                    Popular points of interest near the apartment include Gare de l'Est, Gare du Nord and Opéra Garnier. The nearest airport is Paris - Orly Airport, 17 km from Charming apartment Center of Paris/Montorgueil ( Aboukir 78).
                    <br>
                    This is our guests' favourite part of Paris, according to independent reviews.
                </p>
            </div>
            <div class="col-md-12">
                <div class="card" style="padding: 10px">
                    <div class="card-body">
                        <b style="font-size: 20px">Guest reviews</b>
                        <br>
                        <span class="badge bg-primary" style="margin: 20px 0; font-size: 15px">9</span> Very good · 1 review
                        <br>
                        <b>Categories:</b>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-4" style="font-size: 14px; margin-bottom: 20px">
                                Facilities
                                <div class="progress" style="height: 5px">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-4" style="font-size: 14px; margin-bottom: 20px">
                                Cleanliness
                                <div class="progress" style="height: 5px">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-4" style="font-size: 14px; margin-bottom: 20px">
                                Comfort
                                <div class="progress" style="height: 5px">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-4" style="font-size: 14px; margin-bottom: 20px">
                                Value for money
                                <div class="progress" style="height: 5px">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-4" style="font-size: 14px; margin-bottom: 20px">
                                Location
                                <div class="progress" style="height: 5px">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 90%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 20px">
                <h5><b>House rules</b></h5>
                <span style="font-size: 14px">GuestReady - Parisian dream in Épinettes takes special requests - add in the next step!</span>
                <div class="card" style="padding: 10px; margin-top: 10px">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3" style="font-size: 15px; font-weight: bold">
                                <span><i class="fa-solid fa-right-to-bracket" style="margin-right: 5px; font-size: 20px"></i></span>
                                Check-in
                            </div>
                            <div class="col-md-9" style="line-height: 2; font-size: 14px">
                                From 14:00 to 00:00
                                <br>
                                Guests are required to show a photo identification and credit card upon check-in
                                <br>
                                You'll need to let the property know in advance what time you'll arrive.
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3" style="font-size: 15px; font-weight: bold">
                                <span><i class="fa-solid fa-right-from-bracket" style="margin-right: 5px; font-size: 20px"></i></span>
                                Check-out
                            </div>
                            <div class="col-md-9" style="line-height: 2; font-size: 14px">
                                Until 11:00
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3" style="font-size: 15px; font-weight: bold">
                                <span><i class="fa-solid fa-circle-info" style="margin-right: 5px; font-size: 20px"></i></span>
                                Cancellation/
                                prepayment
                            </div>
                            <div class="col-md-9" style="line-height: 2; font-size: 14px">
                                Cancellation and prepayment policies vary according to apartment type. Please check the apartment conditions when selecting your apartment above.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-2" style="direction: rtl">
                <a class="btn btn-primary" href="/hotel/book">I'll reserve</a>
            </div>
        </div>
    </div>
</div>
