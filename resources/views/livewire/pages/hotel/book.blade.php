<div>
    @livewire('components.header')
    <div class="container-fluid ">
        <div class="container" style="padding: 20px 0">
            <div class="row">
                <div class="col-md-8" style="line-height: 2">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <b style="margin-bottom: 15px">Enter your details</b>
                                <div class="col-md-4">
                                    <label for="first_name" class="form-label">First Name</label>
                                    <input type="text" class="form-control" id="first_name" maxlength="50" placeholder="Enter Your Name" wire:model.lazy="travellers.first_name">
                                    @error('travellers.first_name') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-md-4">
                                    <label for="last_name" class="form-label">Last Name</label>
                                    <input type="text" class="form-control" id="last_name" maxlength="50" placeholder="Enter Your Last Name" wire:model.lazy="travellers.last_name">
                                    @error('travellers.last_name') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-md-4">
                                    <label for="last_name" class="form-label">Email</label>
                                    <input type="text" class="form-control" id="last_name" maxlength="50" placeholder="Enter Your Email" wire:model.lazy="travellers.email">
                                    @error('travellers.email') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            <div class="row" style="margin-top: 15px">
                                <div class="col-md-4">
                                    <span style="font-size: 14px; font-weight: 600">Are you travelling for work?</span>
                                    <br>
                                    <div class="form-check" style="display: inline-block">
                                        <input class="form-check-input" type="radio" name="travel_type" id="travel_type1" style="margin-top: 10px">
                                        <label class="form-check-label" for="travel_type1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check" style="display: inline-block">
                                        <input class="form-check-input" type="radio" name="travel_type" id="travel_type2" checked style="margin-top: 10px">
                                        <label class="form-check-label" for="travel_type2">
                                            No
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <span style="font-size: 14px; font-weight: 600">Who are you booking for?</span>
                                    <br>
                                    <div class="form-check" style="display: inline-block">
                                        <input class="form-check-input" type="radio" name="booking_for" id="booking_for1" style="margin-top: 10px">
                                        <label class="form-check-label" for="booking_for1">
                                            I am the main guest
                                        </label>
                                    </div>
                                    <div class="form-check" style="display: inline-block">
                                        <input class="form-check-input" type="radio" name="booking_for" id="booking_for2" checked style="margin-top: 10px">
                                        <label class="form-check-label" for="booking_for2">
                                            Booking is for someone else
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-4">
                                    <label for="first_name" class="form-label">Guest First Name</label>
                                    <input type="text" class="form-control" id="first_name" maxlength="50" placeholder="Enter Your Name" wire:model.lazy="travellers.first_name">
                                    @error('travellers.first_name') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-md-4">
                                    <label for="last_name" class="form-label">Guest Last Name</label>
                                    <input type="text" class="form-control" id="last_name" maxlength="50" placeholder="Enter Your Last Name" wire:model.lazy="travellers.last_name">
                                    @error('travellers.last_name') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-md-4">
                                    <label for="last_name" class="form-label">Guest Email</label>
                                    <input type="text" class="form-control" id="last_name" maxlength="50" placeholder="Enter Your Email" wire:model.lazy="travellers.email">
                                    @error('travellers.email') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <b style="margin-bottom: 15px">Add to your stay</b>
                            <br>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    <span style="font-size: 13px; font-weight: 400">Want to book a taxi or shuttle ride in advance?</span>
                                    <br>
                                    <span style="font-size: 12px">Avoid surprises - get from the airport to your accommodation without a hitch. We'll add taxi options to your booking confirmation.</span>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault2">
                                <label class="form-check-label" for="flexCheckDefault2">
                                    <span style="font-size: 13px; font-weight: 400">I'm interested in renting a car</span>
                                    <br>
                                    <span style="font-size: 12px">
                                        Make the most out of your trip and check car hire options in your booking confirmation.
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <b style="margin-bottom: 15px">Special requests</b>
                            <br>
                            <span style="font-size: 14px">
                                Special requests cannot be guaranteed – but the property will do its best to meet your needs. You can always make a special request after your booking is complete!
                            </span>
                            <br>
                            <b style="font-size: 14px; font-weight: 600">Please write your requests in English or French. (optional)</b>
                            <br>
                            <textarea class="form-control" rows="3">

                            </textarea>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <b style="margin-bottom: 15px">Review house rules</b>
                            <br>
                            <span style="font-size: 14px">Your host would like you to agree to the following house rules:</span>
                            <ul style="list-style-type: disc; font-size: 13px; margin-top: 15px">
                                <li>No smoking</li>
                                <li>No parties/events</li>
                                <li>Pets are not allowed</li>
                            </ul>
                            <b style="font-size: 14px; font-weight: 600">By continuing to the next step, you are agreeing to these house rules.</b>
                        </div>
                    </div>
                    <div class="mt-2" style="direction: rtl">
                        <button class="btn btn-primary" type="submit">Next</button>
                        <a href="/" class="btn btn-primary">Back</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card tickets-price">
                        <div class="card-body" style="font-size: 15px">
                            <div class="row">
                                <b>Name of Hotel</b>
                                <br>
                                <span style="font-size: 14px">32 Rue Lantiez, 17th arr., 75017 Paris, France</span>
                            </div>
                        </div>
                    </div>
                    <div class="card tickets-price">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7">Checked-in</div>
                                <div class="col-md-5 text-right">Mon 4 Sept 2023</div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">Checked-out</div>
                                <div class="col-md-5 text-right">Thu 7 Sept 2023</div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">Total length of stay:</div>
                                <div class="col-md-5 text-right">3 Nights</div>
                            </div>
                        </div>
                    </div>
                    <div class="card tickets-price">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8"><b>1 Room (1 adult, 1 children)</b></div>
                                <div class="col-md-4 text-right">1 €</div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">Cleaning fee</div>
                                <div class="col-md-4 text-right">1 €</div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">6.25 % City tax</div>
                                <div class="col-md-4 text-right">1 €</div>
                            </div>
                            <div class="row total-price">
                                <div class="col-md-8">Total</div>
                                <div class="col-md-4 text-right">1 €</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
