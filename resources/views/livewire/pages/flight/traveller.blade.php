<div class="">
    @livewire('components.header')
    <div class="container-fluid ">
        <div class="bg-gray-100">
            <div class="container" style="padding: 20px 0">
                <div class="row">
                    <div class="col-md-8">
                        <form wire:submit.prevent="createTravellers" class="flex-col items-center content-center justify-center">
                            <div class="card">
                                <div class="card-body">
                                    <span class="bold"><b>Contacts</b></span>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="phone" class="form-label">Phone</label>
                                            <input type="text" class="form-control" id="phone" oninput="this.value = this.value.replace(/[^0-9+]/g, '').replace(/(\..*?)\..*/g, '$1');" placeholder="Enter Your Name" wire:model.lazy="phone">
                                            @error('phone') <span class="error">{{ $message }}</span> @enderror
                                        </div>
                                        <div class="col-md-3">
                                            <label for="email" class="form-label">Email</label>
                                            <input type="text" class="form-control" id="email" placeholder="Enter Your Name" wire:model.lazy="email">
                                            @error('email') <span class="error">{{ $message }}</span> @enderror
                                        </div>
                                        <div class="col-md-3 pt-5">
                                            <input class="form-check-input" type="checkbox" wire:model="user_information" wire:change="setUserInformation" id="user_informations">
                                            <label class="form-check-label" for="user_informations">
                                                i am the owner
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Show Traveller's Information Form--}}
                            @foreach($travellers as $index => $traveller)
                                <div class="card">
                                    <div class="card-body">
                                        <span class="bold"><b>Traveller {{ $index + 1 }} @if($traveller['age'] == 'Children') ( Child ) @endif</b></span>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="first_name" class="form-label">First Name</label>
                                                <input type="text" class="form-control" id="first_name" maxlength="50" placeholder="Enter Your Name" wire:model.lazy="travellers.{{ $index }}.first_name">
                                                @error('travellers.' . $index . '.first_name') <span class="error">{{ $message }}</span> @enderror
                                            </div>
                                            <div class="col-md-3">
                                                <label for="last_name" class="form-label">Last Name</label>
                                                <input type="text" class="form-control" id="last_name" maxlength="50" placeholder="Enter Your Last Name" wire:model.lazy="travellers.{{ $index }}.last_name">
                                                @error('travellers.' . $index . '.last_name') <span class="error">{{ $message }}</span> @enderror
                                            </div>
                                            <div class="col-md-3">
                                                <label for="gender" class="form-label">Gender</label>
                                                <select class="form-control" id="gender" wire:model.lazy="travellers.{{ $index }}.gender">
                                                    <option selected value="0">Choose Your Gender</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                                @error('travellers.' . $index . '.gender') <span class="error">{{ $message }}</span> @enderror
                                            </div>
                                            <div class="col-md-3">
                                                <label for="date" class="form-label">Birthdate</label>
                                                <input type="date" class="form-control" id="date" max="{{ date('Y-m-d') }}" wire:model.lazy="travellers.{{ $index }}.birthdate">
                                                @error('travellers.' . $index . '.birthdate') <span class="error">{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-12" style="position: relative">
                                                {{-- Show List of Available Baggage--}}
                                                @foreach($baggages as $baggage)
                                                    <div class="form-check">
                                                        <input wire:loading.attr="disabled" class="form-check-input" type="checkbox" id="baggage-{{ $index }}-{{ $baggage->id }}" wire:model.lazy="checked_baggage.{{ $index }}.{{ $baggage->id }}" wire:change="sendBaggageToTravelPriceComponent({{ $index }}, {{ $baggage->id }})">
                                                        <label class="form-check-label" for="baggage-{{ $index }}-{{ $baggage->id }}">
                                                            +{{ $baggage->price }} € - Add {{ $baggage->title }} - Max Weight : {{ $baggage->max_weight }}Kg
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="mt-2" style="direction: rtl">
                                <button class="btn btn-primary" type="submit">Next</button>
                                <a href="/" class="btn btn-primary">Back To Main</a>
                            </div>
                        </form>
                    </div>
                    {{-- Show Price Component--}}
                    <livewire:components.travel_price :travel="$travel" :travellers="$travellers"/>
                </div>
            </div>
        </div>
    </div>
</div>
