<div>
    @livewire('components.header')
    <div class="container-fluid ">
        <div class="bg-gray-100">
            <div class="container" style="padding: 20px 0">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    {{-- Airport and Travel Time Information --}}
                                    <div class="col-md-12">
                                        <img src="/airlines/TK.png" class="float-sm-start mr-4" alt="">
                                        <div class="mt-4">
                                            <span class="mt-5">{{ $origin_city->name }} ({{ $origin_airport->iata_airport_code }}) to {{ $destination_city->name }} ({{ $destination_airport->iata_airport_code }})</span>
                                            <br>
                                            <span style="font-size: 13px">{{  date("d M Y , H:i", strtotime($flight->departure_time)) }} - {{  date("d M Y , H:i", strtotime($flight->arrival_time)) }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="mt-5"><b>Contact details :</b></span>
                                        <br>
                                        <span style="font-size: 13px" class="mt-5">Phone : {{ $travel->phone }}</span>
                                        <br>
                                        <span style="font-size: 13px">Email : {{ $travel->email }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="mt-5"><b>Traveller details :</b></span><br>
                                        {{-- Show All Tickets --}}
                                        @foreach($tickets as $ticket)
                                            <span style="font-size: 13px" class="mt-5">{{ ucfirst($ticket->first_name) }} {{ ucfirst($ticket->last_name) }}</span>
                                            <br>
                                            <span style="font-size: 13px">{{ ucfirst($ticket->age) }} · {{ ucfirst($ticket->gender) }} · {{  date("d M Y", strtotime($ticket->birthdate)) }}</span>
                                            <br>
                                            {{-- Show Baggage's of Tickets --}}
                                            @foreach($ticket->baggage as $baggage)
                                                +
                                                <span style="font-size: 15px; color: #495057">
                                                    <i class="fa-solid fa-suitcase-rolling"></i>
                                                </span>
                                                <span style="font-size: 13px">{{ $baggage->title }} ( Max Weight : {{ $baggage->max_weight }} )</span>
                                                @if(!$loop->last)
                                                    <br>
                                                @endif
                                            @endforeach
                                            @if(!$loop->last)
                                                <hr>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-2" style="direction: rtl">
                            <button class="btn btn-primary" type="submit">Next</button>
                            <a href="/traveller?flight_id={{ $flight->id }}&travel_id={{ $travel->id }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                    {{-- Show Price Component--}}
                    <livewire:components.travel_price :travel="$travel" />
                </div>
            </div>
        </div>
    </div>
</div>
