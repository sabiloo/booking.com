<div class="min-h-screen bg-gray-50 font-normal text-gray-950 antialiased dark:bg-gray-950 dark:text-white">

    <div class="fi-simple-layout flex min-h-screen items-center">
        <div class="fi-simple-main-ctn w-full">
            <main
                class="fi-simple-main mx-auto my-16 w-full bg-white px-6 py-12 shadow-sm ring-1 ring-gray-950/5
                 dark:bg-gray-900 dark:ring-white/10 sm:max-w-lg sm:rounded-xl sm:px-12">
                <div class="fi-simple-page">
                    <section class="grid auto-cols-fr gap-y-6">
                        <header class="fi-simple-header">
                            <div class="mb-4 flex justify-center">
                                <div
                                    class="fi-logo text-xl font-bold leading-5 tracking-tight text-gray-950 dark:text-white">
                                    {{config('app.name')}}
                                </div>
                            </div>

                            <h1 class="fi-simple-header-heading text-center text-2xl font-bold tracking-tight
                            text-gray-950 dark:text-white">
                                Register
                            </h1>

                        </header>

                        <form
                            class="fi-form grid gap-y-6"
                            wire:submit.prevent="register">

                            {{$this->form}}

                            <div class="fi-ac gap-3 grid grid-cols-[repeat(auto-fit,minmax(0,1fr))] fi-form-actions">
                                <button
                                    class=" relative grid-flow-col items-center justify-center
                                    rounded-lg
                                    bg-[#d97706] hover:bg-[#f98804]
                                         font-semibold outline-none transition duration-75
                                          gap-1.5 px-3 py-2 text-sm inline-grid
                                          shadow-sm  text-white  "
                                    type="submit">
                                    <span class="fi-btn-label">
                                        Register
                                    </span>


                                </button>

                            </div>
                        </form>
                    </section>
                </div>
            </main>
        </div>
    </div>
</div>
