<?php

namespace Tests\Feature\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HotelSinglePageRouteTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_hotel_single_page_status_code(): void
    {
        $response = $this->get('/hotel/1/test');
        $response->assertStatus(200);
    }
}
