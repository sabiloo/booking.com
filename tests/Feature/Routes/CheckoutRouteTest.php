<?php

namespace Tests\Feature\Routes;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CheckoutRouteTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_hotel_checkout_page_status_code(): void
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get('/checkout');
        $response->assertStatus(200);
    }
}
