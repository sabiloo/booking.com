<?php

namespace Tests\Feature\Routes;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TravellerRouteTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_traveller_information_page_status_code(): void
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get('/traveller?flight_id=1&travel_id=2&child=1&adult=2');

        $response->assertStatus(200);
    }
}
