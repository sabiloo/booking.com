<?php

namespace Tests\Feature\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HotelBookRouteTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_hotel_booking_page_status_code(): void
    {
        $response = $this->get('/hotel/book');
        $response->assertStatus(200);
    }
}
