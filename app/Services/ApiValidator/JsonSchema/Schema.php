<?php

namespace App\Services\ApiValidator\JsonSchema;

use App\Services\ApiValidator\JsonFieldTypes\BaseType;

class Schema
{
    protected array $schema;
    protected ?bool $statusOfValidation = null;

    public function __construct(array $schema)
    {
        $this->schema = $schema;
    }

    public static function make(array $schema): static
    {
        return app(static::class, ['schema' => $schema]);
    }

    public function getSchema(): array
    {
        return $this->schema;
    }

    public function validate($data): bool
    {
        $statusOfValidation = true;
        /** @var BaseType $value */
        /** @var string $key */
        foreach ($this->schema as $key => $value) {
            if (isset($data[$key])) {
                $statusOfValidation = $value->isValid($data[$key]);
            } else if (!$value->getOptional()) {
                $statusOfValidation = false;
            }

            if (!$statusOfValidation) {
                break;
            }
        }


        $this->statusOfValidation = $statusOfValidation;
        return $this->statusOfValidation;
    }
}
