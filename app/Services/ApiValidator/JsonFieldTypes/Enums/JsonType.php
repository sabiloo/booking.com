<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Enums;

enum JsonType
{
    case ARRAY;
    case BOOL;
    case NULL;
    case NUMBER;
    case OBJECT;
    case STRING;

}
