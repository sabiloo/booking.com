<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Enums;

enum StatusOfValidateValueEnum
{
    case VALID;
    case INVALID;
    case NOT_REVIEWED;
}
