<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Contracts;

interface CanValidate
{
    public function isValid($value): bool;
}
