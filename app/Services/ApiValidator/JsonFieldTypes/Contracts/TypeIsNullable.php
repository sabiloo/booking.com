<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Contracts;

interface TypeIsNullable
{

    public function getNullable(): bool;

    public function nullable(): static;
}
