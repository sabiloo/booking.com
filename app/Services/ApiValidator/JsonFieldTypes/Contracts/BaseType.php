<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Contracts;

interface BaseType
{
    public function __construct(string $key);
    public static function key(string $name): static;
}
