<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Contracts;

use App\Services\ApiValidator\JsonFieldTypes\Enums\JsonType;

interface HasType
{
    public function getType(): JsonType;

}
