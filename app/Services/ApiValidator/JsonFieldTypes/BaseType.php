<?php

namespace App\Services\ApiValidator\JsonFieldTypes;

use App\Enums\Role;
use App\Services\ApiValidator\JsonFieldTypes\Contracts\CanValidate;
use App\Services\ApiValidator\JsonFieldTypes\Contracts\HasType;
use App\Services\ApiValidator\JsonFieldTypes\Enums\StatusOfValidateValueEnum;
use \App\Services\ApiValidator\JsonFieldTypes\Contracts\BaseType as BaseTypeInterface;

abstract class BaseType
    implements BaseTypeInterface,
    HasType,
    CanValidate
{
    protected string $key;

    protected bool $optional = false;
    protected mixed $originalValue;
    protected StatusOfValidateValueEnum $statusOfValidateValue = StatusOfValidateValueEnum::NOT_REVIEWED;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    public static function key(string $name): static
    {
        return app(static::class, ['key' => $name]);
    }

    public function optional(): static
    {
        $this->optional = true;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getOptional(): bool
    {
        return $this->optional;
    }

    public function getOriginalValue()
    {
        return $this->originalValue;
    }

    public function getStatusOfValidateValue(): StatusOfValidateValueEnum
    {
        return $this->statusOfValidateValue;
    }

}
