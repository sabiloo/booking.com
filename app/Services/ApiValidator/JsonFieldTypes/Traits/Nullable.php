<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Traits;

trait Nullable
{

    protected bool $nullable = false;

    public function getNullable(): bool
    {
        return $this->nullable;
    }

    public function nullable(): static
    {
        $this->nullable = true;
        return $this;
    }
}
