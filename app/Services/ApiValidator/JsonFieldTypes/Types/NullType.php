<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Types;

use App\Services\ApiValidator\JsonFieldTypes\BaseType;
use App\Services\ApiValidator\JsonFieldTypes\Enums\JsonType;

class NullType extends BaseType
{
    const  TYPE = JsonType::NULL;

    public function getType(): JsonType
    {
        return self::TYPE;
    }
    public function isValid($value): bool
    {
//        TODO
        return true;
    }
}
