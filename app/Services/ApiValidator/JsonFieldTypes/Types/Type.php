<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Types;

class Type
{

    public static function array(string $key): ArrayType
    {
        return ArrayType::key($key);
    }

    public static function bool(string $key): BoolType
    {
        return BoolType::key($key);
    }

    public static function null(string $key): NullType
    {
        return NullType::key($key);
    }

    public static function number(string $key): NumberType
    {
        return NumberType::key($key);
    }

    public static function object(string $key): ObjectType
    {
        return ObjectType::key($key);
    }

    public static function string(string $key): StringType
    {
        return StringType::key($key);
    }
}
