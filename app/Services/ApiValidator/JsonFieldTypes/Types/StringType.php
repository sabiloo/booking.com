<?php

namespace App\Services\ApiValidator\JsonFieldTypes\Types;


use App\Services\ApiValidator\JsonFieldTypes\BaseType;
use App\Services\ApiValidator\JsonFieldTypes\Contracts\TypeIsNullable;
use App\Services\ApiValidator\JsonFieldTypes\Enums\JsonType;
use App\Services\ApiValidator\JsonFieldTypes\Traits\Nullable;

class StringType extends BaseType
    implements TypeIsNullable
{
    use Nullable;

    const  TYPE = JsonType::STRING;

    public function getType(): JsonType
    {
        return self::TYPE;
    }

    public function isValid($value): bool
    {
//        TODO
        return true;
    }
}
