<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception|Throwable $exception)
    {

        if ($request->wantsJson()) {

            if ($exception instanceof ModelNotFoundException) {
                return response(['message' => 'آیتم مورد نظر یافت نشد'],404);
            }

            if ($exception instanceof NotFoundHttpException){
                return response(['message' => 'آدرس مورد نظر پیدا نشد'],404);
            }


        }

        return parent::render($request, $exception);
    }

}
