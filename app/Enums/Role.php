<?php

namespace App\Enums;

enum Role: string
{
    case ADMIN = 'admin';
    case CLIENT = 'client';
    case TRAVEL_AGENCY = 'travel_agency';

}
