<?php

namespace App\Providers;

use App\Repository\AirlineRepositoryInterface;
use App\Repository\AirportRepositoryInterface;
use App\Repository\BaggageRepositoryInterface;
use App\Repository\CityRepositoryInterface;
use App\Repository\Eloquent\AirlineRepository;
use App\Repository\Eloquent\AirportRepository;
use App\Repository\Eloquent\BaggageRepository;
use App\Repository\Eloquent\BaseRepository;
use App\Repository\Eloquent\CityRepository;
use App\Repository\Eloquent\FlightRepository;
use App\Repository\Eloquent\TicketRepository;
use App\Repository\Eloquent\TravelRepository;
use App\Repository\EloquentRepositoryInterface;
use App\Repository\FlightRepositoryInterface;
use App\Repository\TicketRepositoryInterface;
use App\Repository\TravelRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(FlightRepositoryInterface::class, FlightRepository::class);
        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);
        $this->app->bind(AirportRepositoryInterface::class, AirportRepository::class);
        $this->app->bind(TicketRepositoryInterface::class, TicketRepository::class);
        $this->app->bind(TravelRepositoryInterface::class, TravelRepository::class);
        $this->app->bind(BaggageRepositoryInterface::class, BaggageRepository::class);
        $this->app->bind(AirlineRepositoryInterface::class, AirlineRepository::class);

    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
