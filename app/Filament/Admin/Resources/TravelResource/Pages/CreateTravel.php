<?php

namespace App\Filament\Admin\Resources\TravelResource\Pages;

use App\Filament\Admin\Resources\TravelResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTravel extends CreateRecord
{
    protected static string $resource = TravelResource::class;
}
