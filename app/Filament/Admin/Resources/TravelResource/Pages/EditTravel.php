<?php

namespace App\Filament\Admin\Resources\TravelResource\Pages;

use App\Filament\Admin\Resources\TravelResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTravel extends EditRecord
{
    protected static string $resource = TravelResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
