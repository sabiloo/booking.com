<?php

namespace App\Filament\Admin\Resources\FlightResource\Pages;

use App\Filament\Admin\Resources\FlightResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateFlight extends CreateRecord
{
    protected static string $resource = FlightResource::class;
}
