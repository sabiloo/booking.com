<?php

namespace App\Filament\Admin\Resources\TravellerResource\Pages;

use App\Filament\Admin\Resources\TravellerResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTraveller extends EditRecord
{
    protected static string $resource = TravellerResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
