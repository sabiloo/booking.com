<?php

namespace App\Filament\Admin\Resources\TravellerResource\Pages;

use App\Filament\Admin\Resources\TravellerResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTravellers extends ListRecords
{
    protected static string $resource = TravellerResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
