<?php

namespace App\Filament\Admin\Resources\TravellerResource\Pages;

use App\Filament\Admin\Resources\TravellerResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTraveller extends CreateRecord
{
    protected static string $resource = TravellerResource::class;
}
