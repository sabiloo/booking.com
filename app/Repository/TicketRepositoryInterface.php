<?php

namespace App\Repository;

use App\Models\Ticket;

interface TicketRepositoryInterface
{
    public function create(array $params);

    public function update($id, array $params);

    public function addBaggageToTicket(Ticket $ticket, $params);

    public function getTicketsOfTravel($travel_id);
}
