<?php

namespace App\Repository;

interface AirportRepositoryInterface
{
    public function findByCity($cityId) : array;

    public function findById($id);
}
