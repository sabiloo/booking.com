<?php

namespace App\Repository\Eloquent;

use App\Models\Baggage;
use App\Repository\BaggageRepositoryInterface;

class BaggageRepository extends BaseRepository implements BaggageRepositoryInterface
{
    public function __construct(Baggage $model)
    {
        parent::__construct($model);
    }

    public function all()
    {
        return $this->model->all();
    }
}
