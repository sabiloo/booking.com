<?php

namespace App\Repository\Eloquent;

use App\Models\City;
use App\Repository\CityRepositoryInterface;

class CityRepository extends BaseRepository implements CityRepositoryInterface
{

    public function __construct(City $model)
    {
        parent::__construct($model);
    }

    public function searchInName($q)
    {
        return $this->model->query()->where('name', 'like', "%{$q}%")->get();
    }
}
