<?php

namespace App\Repository\Eloquent;

use App\Models\Airline;
use App\Repository\AirlineRepositoryInterface;

class AirlineRepository extends BaseRepository implements AirlineRepositoryInterface
{
    public function __construct(Airline $model)
    {
        parent::__construct($model);
    }

    public function findById($id)
    {
        return $this->model->query()->find($id);
    }
}
