<?php

namespace App\Repository\Eloquent;

use App\Models\Airport;
use App\Models\Flight;
use App\Repository\AirportRepositoryInterface;

class AirportRepository extends BaseRepository implements AirportRepositoryInterface
{
    public function __construct(Airport $model)
    {
        parent::__construct($model);
    }

    public function findByCity($cityId) : array
    {
        return $this->query()->where('city_id', $cityId)->get()->toArray();
    }

    public function findById($id)
    {
        return $this->model->query()->where('id', $id)->first();
    }
}
