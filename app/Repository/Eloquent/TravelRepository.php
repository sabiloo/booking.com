<?php

namespace App\Repository\Eloquent;

use App\Models\Travel;
use App\Models\User;
use App\Repository\TravelRepositoryInterface;

class TravelRepository extends BaseRepository implements TravelRepositoryInterface
{
    public function __construct(Travel $model)
    {
        parent::__construct($model);
    }

    public function create($attributes)
    {
        return $this->model->create($attributes);
    }

    public function update(Travel $travel, $attributes)
    {
        return $travel->update($attributes);
    }

    public function findById($id)
    {
        return $this->model->query()->where('id', $id)->first();
    }

    public function getLastTravelOfUser($user_id)
    {
        return $this->model->where('user_id', $user_id)->latest('id')->first();
    }
}
