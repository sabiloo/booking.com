<?php

namespace App\Repository\Eloquent;

use App\Models\Ticket;
use App\Repository\TicketRepositoryInterface;

class TicketRepository extends BaseRepository implements TicketRepositoryInterface
{
    public function __construct(Ticket $model)
    {
        parent::__construct($model);
    }

    public function create($params)
    {
        return $this->model->create($params);
    }

    public function update($id, $params)
    {
        $ticket = Ticket::where('id', $id)->first();
        $ticket->update($params);
        return $ticket;
    }

    public function addBaggageToTicket(Ticket $ticket, $params)
    {
        $ticket->baggage()->sync($params);
    }

    public function getTicketsOfTravel($travel_id)
    {
        return $this->model->query()->where('travel_id', $travel_id)->orderBy('id', 'ASC')
            ->with('baggage')->get();
    }
}
