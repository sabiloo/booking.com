<?php

namespace App\Repository\Eloquent;

use App\Models\Airport;
use App\Models\Flight;
use App\Repository\FlightRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use JetBrains\PhpStorm\ArrayShape;

class FlightRepository extends BaseRepository implements FlightRepositoryInterface
{
    public function __construct(Flight $model)
    {
        parent::__construct($model);
    }

    /**
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Collection
     */
    #[ArrayShape([
        'origin' => 'int',
        'destination' => 'int',
        'departure_date' => 'string',
        'number_of_seats' => 'int',
        'cabin_type' => 'string'
    ])]
    public function search(array $params)
    {
        $flightQuery = $this->model::query();

        $flightQuery->whereIn('origin_airport_id', $this->getAirportsId($params['origin']));
        $flightQuery->whereIn('destination_airport_id', $this->getAirportsId($params['destination']));

        if (Carbon::make($params['departure_date'])->isToday()) {
            $flightQuery->whereBetween('departure_time',
                [
                    Carbon::now()->setTimeZone('Asia/Tehran')->format('Y-m-d H:i:s'),
                    Carbon::now()->setTimeZone('Asia/Tehran')->endOfDay()->format('Y-m-d H:i:s')
                ]
            );
        } else {
            $flightQuery->whereBetween('departure_time',
                [
                    Carbon::make($params['departure_date'])->setTimeZone('Asia/Tehran')->startOfDay()->format('Y-m-d H:i:s'),
                    Carbon::make($params['departure_date'])->setTimeZone('Asia/Tehran')->endOfDay()->format('Y-m-d H:i:s')
                ]
            );
        }

        $flightQuery->where('available_seats', '>=', $params['number_of_seats']);

        $flightQuery->where('cabin_type', $params['cabin_type']);

        return $flightQuery->get();
    }

    protected function getAirportsId($cityId) : array
    {
        return Airport::query()->where('city_id', $cityId)->get('id')->toArray();
    }

    public function findById($id)
    {
        $flight = $this->model->query()->where('id', $id)->first();
        if($flight)
            return $flight;
        else
            abort(404);
    }

    public function getFlightByTravelId($travel_id)
    {
        return $this->model->query()->where('travel_id', $travel_id)->first();
    }

    public function findOriginCity(Flight $flight)
    {
        return $flight->airport_origin;
    }

    public function findDestinationCity(Flight $flight)
    {
        return $flight->airport_destination;
    }
}
