<?php

namespace App\Repository;

interface BaggageRepositoryInterface
{
    public function all();
}
