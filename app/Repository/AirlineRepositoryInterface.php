<?php

namespace App\Repository;

interface AirlineRepositoryInterface
{
    public function findById($id);
}
