<?php

namespace App\Repository;

interface CityRepositoryInterface
{
    public function searchInName($q);
}
