<?php

namespace App\Repository;

use App\Models\Flight;
use Illuminate\Support\Collection;

interface FlightRepositoryInterface
{
    public function search(array $params);

    public function findById($id);

    public function getFlightByTravelId($travel_id);

    public function findOriginCity(Flight $flight);

    public function findDestinationCity(Flight $flight);
}
