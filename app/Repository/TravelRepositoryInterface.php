<?php

namespace App\Repository;

use App\Models\Travel;
use App\Models\User;

interface TravelRepositoryInterface
{
    public function create(array $params);

    public function update(Travel $travel, array $params);

    public function findById($id);

    public function getLastTravelOfUser($user_id);
}
