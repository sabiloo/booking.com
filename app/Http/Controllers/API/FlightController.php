<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\FlightRequest;
use App\Http\Resources\FlightCollection;
use App\Repository\FlightRepositoryInterface;

class FlightController extends Controller
{
    private FlightRepositoryInterface $flightRepository;

    public function __construct(FlightRepositoryInterface $flightRepository)
    {
        $this->flightRepository = $flightRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(FlightRequest $request)
    {
        return new FlightCollection($this->flightRepository->search($request->all()));
    }
}
