<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class FlightRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'origin' => 'required|exists:airports,city_id',
            'destination' => 'required|exists:airports,city_id',
            'departure_date' => 'required|date|after_or_equal:today',
            'travellers' => 'required|array|min:1',
            'cabin_type' => 'required|in:economy,business,first-class'
        ];
    }
}
