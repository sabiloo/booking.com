<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FlightResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'airline' => new AirlineResource($this->airline),
            'origin_airport' => new AirportResource($this->origin),
            'destination_airport' => new AirportResource($this->destination),
            'available_seats' => $this->available_seats,
            'cabin_type' => $this->cabin_type,
            'departure_time' => $this->departure_time,
            'arrival_time' => $this->arrival_time,
            'price' => $this->price,
            'is_insurance' => $this->is_insurance

        ];
    }
}
