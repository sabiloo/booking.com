<?php

namespace App\Livewire;

use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Form;
use Livewire\Component;

class HotelSearch extends Component implements HasForms
{
    use InteractsWithForms;


    public ?array $data = [];

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([

                Select::make('destination')
                    ->required()
                    ->searchable()
                    ->options(['azsd', 'asd']),

                DatePicker::make('check_in')
                    ->minDate(now())
                    ->maxDate(now()->addYear()),
                DatePicker::make('check_out')
                    ->minDate(now()->addDay())
                    ->maxDate(now()->addYear()),

                TextInput::make('adults')
                    ->required()
                    ->integer()
                    ->minValue(1)
                    ->maxValue(30)
                    ->default(1),
                TextInput::make('children')
                    ->required()
                    ->integer()
                    ->minValue(0)
                    ->maxValue(20)
                    ->default(0),
                TextInput::make('rooms')
                    ->required()
                    ->integer()
                    ->minValue(1)
                    ->maxValue(30)
                    ->default(1),
            ])->columns(3);
    }

    public function search(): void
    {
        dd($this->form->getState());
    }


    public function render()
    {
        return view('livewire.hotel-search');
    }

}
