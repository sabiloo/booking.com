<?php

namespace App\Livewire\Flights;

use Illuminate\Support\Facades\Http;
use Livewire\Component;
use GuzzleHttp\Client;

class Search extends Component
{
    public $origin = 'test1';
    public $destination = 'test2';
    public $date;
    public $traveller_count;


    public $data;
    public $result;

    public function mount()
    {
        $data = [
            'origin' => $this->origin,
            'destination' => $this->destination,
            'date' => date('Y-m-d'),
            'traveller_count' => rand(1,9)
        ];

        $flights = resolve('flights');
        $response = $flights->searchInFlights($data);

        if ($response->successful()) {
            // Success
            $this->result = $response->json();
        } else {
            // Failed
            dd('failed');
        }
    }

    public function search()
    {
        $data = [
            'origin' => $this->origin,
            'destination' => $this->destination,
            'date' => date('Y-m-d'),
            'traveller_count' => rand(1,9)
        ];

        $response = Http::post('localhost/api/search', $data);

        if ($response->successful()) {
            // Success
            $this->result = $response->json();
        } else {
            // Failed
            dd('failed');
        }
    }

    public function render()
    {
        return view('livewire.flights.search');
    }
}
