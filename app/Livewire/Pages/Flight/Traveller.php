<?php

namespace App\Livewire\Pages\Flight;

use App\Repository\BaggageRepositoryInterface;
use App\Repository\FlightRepositoryInterface;
use App\Repository\TicketRepositoryInterface;
use App\Repository\TravelRepositoryInterface;
use Livewire\Component;

class Traveller extends Component
{

    public $phone;
    public $email;
    public $travel;
    public $flight;
    public $baggages;
    public $user_information;
    public $price;
    public $checked_baggage = [];
    public $travellers = [];

    protected $rules = [
        'travellers.*.first_name' => 'required|min:3|max:50|regex:/^[a-zA-Z ]+$/u',
        'travellers.*.last_name' => 'required|min:3|max:50|regex:/^[a-zA-Z]+$/u',
        'travellers.*.gender' => 'required|not_in:0',
        'travellers.*.birthdate' => 'required',
        'phone' => 'required|numeric',
        'email' => 'required|email',
    ];

    protected $messages = [
        'travellers.*.first_name.required' => 'The First Name cannot be empty.',
        'travellers.*.last_name.required' => 'The First Name cannot be empty.',
        'travellers.*.first_name.regex' => 'The First Name field must only contain letters.',
        'travellers.*.last_name.regex' => 'The Last Name field must only contain letters.',
        'travellers.*.first_name.min' => 'The First Name must be at least 3 characters..',
        'travellers.*.last_name.min' => 'The Last Name must be at least 3 characters..',
        'travellers.*.first_name.max' => 'The First Name cannot be empty.',
        'travellers.*.last_name.max' => 'The Last Name cannot be empty.',
        'travellers.*.gender.required' => 'The Gender cannot be empty.',
        'travellers.*.gender.not_in' => 'The Gender cannot be empty.',
        'travellers.*.birthdate.required' => 'The Birthdate cannot be empty.',
        'phone.required' => 'The Phone cannot be empty.',
        'phone.regex' => 'Please Enter a Valid Phone.',
        'email.required' => 'The Email cannot be empty.',
        'email.email' => 'Please Enter a Valid Email.',
    ];
    private TicketRepositoryInterface $ticketRepository;
    private TravelRepositoryInterface $travelRepository;
    private FlightRepositoryInterface $flightRepository;
    private BaggageRepositoryInterface $baggageRepository;

    public function boot(TicketRepositoryInterface $ticketRepository, TravelRepositoryInterface $travelRepository, FlightRepositoryInterface $flightRepository, BaggageRepositoryInterface $baggageRepository)
    {
        $this->ticketRepository = $ticketRepository;
        $this->travelRepository = $travelRepository;
        $this->flightRepository = $flightRepository;
        $this->baggageRepository = $baggageRepository;
    }

    public function mount()
    {
        $this->travel = $this->travelRepository->findById(request('travel_id'));

        $this->flight = $this->flightRepository->findById(request('flight_id'));

        $this->baggages = $this->baggageRepository->all();

        $tickets = $this->ticketRepository->getTicketsOfTravel($this->travel->id);

        //---------------- Check Travel Have Tickets Or Not ----------------//
        //---------------- if Count Tickets != 0 Then User Want To Edit Information ----------------//
        if (count($tickets) === 0) {
            //---------------- Add Adult Property To Traveller Array ----------------//
            for ($i = 0; $i < request('adult'); $i++) {
                $this->travellers[] = [
                    'age' => 'adult',
                ];
            }

            //---------------- Add Children Property To Traveller Array ----------------//
            for ($i = 0; $i < request('child'); $i++) {
                $this->travellers[] = [
                    'age' => 'children',
                ];
            }
        } else {
            foreach ($tickets as $index => $ticket) {
                $baggage = [];
                //---------------- Create Baggage Array ----------------//
                foreach ($ticket->baggage as $item) {
                    $baggage[$item->id] = $item->id;
                    $this->checked_baggage[$index][$item->id] = true;
                }
                //---------------- Add Previous Traveller's Information To Array ----------------//
                $this->travellers[] = [
                    'id' => $ticket->id,
                    'first_name' => $ticket->first_name,
                    'last_name' => $ticket->last_name,
                    'gender' => $ticket->gender,
                    'birthdate' => $ticket->birthdate,
                    'age' => $ticket->age,
                    'baggage' => $baggage,
                ];
            }
            $this->phone = $this->travel->phone;
            $this->email = $this->travel->email;
        }
    }

    //---------------- Real Time Validation ----------------//
    public function updated($value)
    {
        $this->resetValidation($value);
        $this->resetErrorBag($value);
    }

    //---------------- Create Ticket and Update Travel's Contact ----------------//
    public function createTravellers()
    {
        $this->validate();

        //---------------- Update Travel Contacts ----------------//
        $this->travelRepository->update($this->travel, [
            'phone' => $this->phone,
            'email' => $this->email,
        ]);

        //---------------- Create Tickets ----------------//
        foreach ($this->travellers as $traveller) {
            if (isset($traveller['id'])) {
                $ticket = $this->ticketRepository->update($traveller['id'], [
                    'gender' => $traveller['gender'],
                    'first_name' => $traveller['first_name'],
                    'last_name' => $traveller['last_name'],
                    'birthdate' => $traveller['birthdate'],
                ]);
            } else {
                $ticket = $this->ticketRepository->create([
                    'travel_id' => $this->travel->id,
                    'gender' => $traveller['gender'],
                    'first_name' => $traveller['first_name'],
                    'last_name' => $traveller['last_name'],
                    'birthdate' => $traveller['birthdate'],
                    'age' => $traveller['age'],
                    'is_insurance' => 1,
                ]);
            }

            //---------------- Add Baggage To Ticket ----------------//
            if (isset($traveller['baggage'])) {
                $this->ticketRepository->addBaggageToTicket($ticket, $traveller['baggage']);
            }
        }

        return $this->redirect(route('checkout'));
    }

    //---------------- Set Information Of The Logged-In User To Travel's Contact Inputs ----------------//
    public function setUserInformation()
    {
        if ($this->user_information) {
            $this->phone = auth()->user()->phone;
            $this->email = auth()->user()->email;
        } else {
            $this->phone = '';
            $this->email = '';
        }
    }

    //---------------- Send Selected Baggage To Travel's Price Component ----------------//
    public function sendBaggageToTravelPriceComponent($index, $baggage_id)
    {
        if ($this->checked_baggage[$index][$baggage_id]) {
            //---------------- Add Baggage's Price to Total ----------------//
            $this->dispatch('baggageAdded', baggage_id: $baggage_id);
            $this->travellers[$index]['baggage'][$baggage_id] = $baggage_id;
        } else {
            //---------------- Reduce Baggage's Price to Total ----------------//
            $this->dispatch('baggageRemoved', baggage_id: $baggage_id);
            unset($this->travellers[$index]['baggage'][$baggage_id]);
        }
    }

    public function render()
    {
        return view('livewire.pages.flight.traveller');
    }
}
