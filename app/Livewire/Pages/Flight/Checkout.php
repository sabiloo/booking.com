<?php

namespace App\Livewire\Pages\Flight;

use App\Repository\AirlineRepositoryInterface;
use App\Repository\AirportRepositoryInterface;
use App\Repository\FlightRepositoryInterface;
use App\Repository\TicketRepositoryInterface;
use App\Repository\TravelRepositoryInterface;
use Livewire\Component;

class Checkout extends Component
{
    public $user;
    public $travel;
    public $flight;
    public $airline;
    public $price;
    public $tickets;
    public $origin_city;
    public $destination_city;
    public $origin_airport;
    public $destination_airport;
    private TicketRepositoryInterface $ticketRepository;
    private TravelRepositoryInterface $travelRepository;
    private FlightRepositoryInterface $flightRepository;
    private AirlineRepositoryInterface $airlineRepository;
    private AirportRepositoryInterface $airportRepository;

    public function boot(TicketRepositoryInterface $ticketRepository, TravelRepositoryInterface $travelRepository, FlightRepositoryInterface $flightRepository, AirlineRepositoryInterface $airlineRepository, AirportRepositoryInterface $airportRepository)
    {
        $this->ticketRepository = $ticketRepository;
        $this->travelRepository = $travelRepository;
        $this->flightRepository = $flightRepository;
        $this->airlineRepository = $airlineRepository;
        $this->airportRepository = $airportRepository;
    }

    public function mount()
    {
        $this->user = auth()->user();
        $this->travel = $this->travelRepository->getLastTravelOfUser($this->user->id);
        $this->flight = $this->flightRepository->findById($this->travel->flight_id);
        $this->airline = $this->airlineRepository->findById($this->flight->airline_id);
        $this->tickets = $this->ticketRepository->getTicketsOfTravel($this->travel->id);
        $this->origin_airport = $this->flight->airport_origin;
        $this->destination_airport = $this->flight->airport_destination;
        $this->origin_city = $this->flightRepository->findOriginCity($this->flight);
        $this->destination_city = $this->flightRepository->findDestinationCity($this->flight);
    }

    public function render()
    {
        return view('livewire.pages.flight.checkout');
    }
}
