<?php

namespace App\Livewire\Pages\Hotel;

use Livewire\Component;

class Book extends Component
{
    public function render()
    {
        return view('livewire.pages.hotel.book');
    }
}
