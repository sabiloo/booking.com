<?php

namespace App\Livewire\Pages\Hotel;

use Livewire\Component;

class Reservation extends Component
{
    public function render()
    {
        return view('livewire.pages.hotel.reservation');
    }
}
