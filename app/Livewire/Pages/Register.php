<?php

namespace App\Livewire\Pages;

use App\Enums\Role;
use App\Models\User;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Wizard;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Register extends Component implements HasForms
{
    use InteractsWithForms;

    public ?array $data = [];
    public string $first_name;
    public string $last_name;
    public string $email;
    public string $password;

    public function mount()
    {
//        $this->form->fill();
    }

    protected function getFormStatePath(): string
    {
        return 'data';
    }

    protected function getFormSchema(): array
    {
        return [

            TextInput::make('first_name')
                ->required()
                ->maxLength(50),
            TextInput::make('last_name')
                ->required()
                ->maxLength(50),

            TextInput::make('email')
                ->label('Email Address')
                ->email()
                ->required()
                ->maxLength(50)
                ->unique(User::class),

            TextInput::make('password')
                ->label('Password')
                ->password()
                ->required()
                ->maxLength(50)
                ->minLength(6)
                ->same('password_confirmation')
                ->confirmed()
                ->dehydrateStateUsing(fn($state) => Hash::make($state)),

            TextInput::make('password_confirmation')
                ->label('Confirm Password')
                ->password()
                ->required()
                ->maxLength(50)
                ->minLength(6)
                ->dehydrated(false),

        ];
    }

    public function register()
    {
        $user = User::create($this->form->getState());
        $user->assignRole(Role::CLIENT->value);
        auth()->login($user);
        $this->redirectRoute('index');
    }

    public function render()
    {
        return view('livewire.pages.register');
    }
}
