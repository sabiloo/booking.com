<?php

namespace App\Livewire\Components;

use App\Models\Flight;
use App\Repository\Eloquent\FlightRepository;
use App\Repository\FlightRepositoryInterface;
use Livewire\Attributes\Url;
use Livewire\Component;

class FlightSearchResult extends Component
{

    #[Url]
    public $ticket_class;
    #[Url]
    public $adult_passengers;
    #[Url]
    public $children_passengers;
    #[Url]
    public $from;
    #[Url]
    public $to;
    #[Url]
    public $date;

    public $flights;

    public function mount()
    {


//        $receivedData = [
//            $this->ticket_class,
//            $this->adult_passengers,
//            $this->children_passengers,
//            $this->from,
//            $this->to,
//            $this->date,
//        ];
//        dump($receivedData);

        if (in_array(null, [
            $this->ticket_class,
            $this->adult_passengers,
            $this->children_passengers,
            $this->from,
            $this->to,
            $this->date,
        ], true)) {
            return;
        }
        $flightRepository = new FlightRepository(new Flight);
        $searchData = [
            'origin' => $this->from,
            'destination' => $this->to,
            'departure_date' => $this->date,
            'number_of_seats' => $this->adult_passengers + $this->children_passengers,
            'cabin_type' => $this->ticket_class,
        ];
//        $searchData = [
//            'origin' => 61800,
//            'destination' => 61800,
//            'departure_date' => '2023-08-20',
//            'number_of_seats' => 1,
//            'cabin_type' => 'economy',
//        ];
        
    $this->flights = $flightRepository->search($searchData);

    }

    public function render()
    {
        return view('livewire.components.flight-search-result');
    }
}
