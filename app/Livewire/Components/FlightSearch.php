<?php

namespace App\Livewire\Components;

use App\Models\City;
use Filament\Forms\Components;
use Filament\Forms\Components\Select;
use Livewire\Attributes\Url;
use Livewire\Component;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Form;
use Illuminate\Contracts\View\View;


class FlightSearch extends Component implements HasForms
{
    use InteractsWithForms;

    public ?array $data = [];
    #[Url]
    public $ticket_class;
    #[Url]
    public $adult_passengers;
    #[Url]
    public $children_passengers;
    #[Url]
    public $from;
    #[Url]
    public $to;
    #[Url]
    public $date;

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Components\Grid::make(3)->schema([
                    Select::make('ticket_class')
                        ->required()
                        ->default($this->ticket_class)
//                        TODO
                        ->options([
                            'economy' => 'Economy',
                            'business' => 'Business',
                            'first_class' => 'First Class',
                        ]),

                    TextInput::make('adult_passengers')
                        ->required()
                        ->default($this->adult_passengers)
                        ->integer()
                        ->minValue(1)
                        ->default(1),

                    TextInput::make('children_passengers')
                        ->default($this->children_passengers)
                        ->required()
                        ->integer()
                        ->minValue(0)
                        ->default(0),
                ]),
                Components\Grid::make(3)->schema([
                    Select::make('from')
                        ->required()
                        ->searchable()
                        ->getSearchResultsUsing(fn(string $search): array => City::where('name', 'like', "%{$search}%")
                            ->orderBy('name')
                            ->limit(50)
                            ->pluck('name', 'id')->toArray()),
                    Select::make('to')
                        ->required()
                        ->searchable()
                        ->getSearchResultsUsing(fn(string $search): array => City::where('name', 'like', "%{$search}%")
                            ->orderBy('name')
                            ->limit(50)
                            ->pluck('name', 'id'
                            )->toArray()),
                    Components\DatePicker::make('date')
                        ->default($this->date)
                        ->required()
                        ->minDate(now())
                        ->maxDate(now()->addYear())

                ]),

            ])
            ->statePath('data');
    }

    public function search(): void
    {
//ticket_class
//adult_passengers
//children_passengers
//from
//to
//date
        $this->redirectRoute('index', $this->form->getState());
//        dd($this->form->getState());

    }

    public function render()
    {
        return view('livewire.components.flight-search');
    }
}
