<?php

namespace App\Livewire\Components;

use App\Repository\TravelRepositoryInterface;
use Livewire\Component;

class FlightCardItem extends Component
{
    public $flight_id;
    public $travel;
    private TravelRepositoryInterface $travelRepository;

    public function boot(TravelRepositoryInterface $travelRepository)
    {
        $this->travelRepository = $travelRepository;
    }
    public function flight_detail()
    {
        $this->travel = $this->travelRepository->create([
            'flight_id' => $this->flight_id,
            'user_id'   => 1,
        ]);
        return redirect(url('/traveller' . '?flight_id=' . $this->flight_id . '&travel_id=' . $this->travel->id . '&child=1&adult=2'));
    }
    public function render()
    {
        return view('livewire.components.flight-card-item');
    }
}
