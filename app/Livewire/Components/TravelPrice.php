<?php

namespace App\Livewire\Components;

use App\Models\Baggage;
use Livewire\Attributes\On;
use Livewire\Component;

class TravelPrice extends Component
{
    public $travel;
    public $travellers = [];
    public $price;

    public function mount()
    {
        //---------------- Calculate Price of Travel ----------------//
        $this->price = $this->travel->calculate_travel_price($this->travellers);
    }

    #[On('baggageAdded')]
    public function baggageAdded($baggage_id)
    {
        $baggage = Baggage::where('id', $baggage_id)->pluck('price')->first();
        $this->price['baggage'] += (int) $baggage;
        $this->price['total'] += (int) $baggage;
    }

    #[On('baggageRemoved')]
    public function baggageRemoved($baggage_id)
    {
        $baggage = Baggage::where('id', $baggage_id)->pluck('price')->first();
        $this->price['baggage'] -= (int) $baggage;
        $this->price['total'] -= (int) $baggage;
    }

    public function render()
    {
        return view('livewire.components.travel-price');
    }
}
