<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Filament\Models\Contracts\HasAvatar;
use Filament\Models\Contracts\HasName;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
    implements
    HasAvatar,
    HasName
{
    use HasApiTokens,
        HasFactory,
        Notifiable,
        HasRoles;

    public function travels()
    {
        return $this->hasMany(Travel::class);
    }


    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function getFilamentAvatarUrl(): ?string
    {
        return $this->avatar_image_path ?
            asset('storage/' . $this->avatar_image_path) : null;

    }

    public function getFilamentName(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
