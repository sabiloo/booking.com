<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    use HasFactory;

    protected $table = 'travels';

    protected $fillable = ['user_id', 'flight_id', 'email', 'phone'];

    public function flight(){
        return $this->belongsTo(Flight::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }

    public function calculate_travel_price($travellers = [])
    {
        $baggage_price = 0;

        //---------------- Check Travel Have Tickets or Not ----------------//
        $tickets = $this->tickets();
        if($tickets->count() != 0) {

            //---------------- if Travel Have Ticket , Calculate Ticket's Price and Baggage ----------------//
            foreach ($tickets->get() as $ticket) {
                foreach ($ticket->baggage as $baggage) {

                    //---------------- Calculate Baggage Price ----------------//
                    $baggage_price += $baggage->price;
                }
            }
            return [
                'fair'      => $this->flight->price * $tickets->count(),
                'taxes'     => $this->flight->taxes * $tickets->count(),
                'total'     => $this->flight->price * $tickets->count() + $this->flight->taxes * $tickets->count() + $baggage_price,
                'baggage'   => $baggage_price
            ];
        } else {

            //---------------- if Travel Have not any Ticket , Just Calculate Ticket's Price ----------------//
            return [
                'fair'      => $this->flight->price * count($travellers),
                'taxes'     => $this->flight->taxes * count($travellers),
                'total'     => $this->flight->price * count($travellers) + $this->flight->taxes * count($travellers) + $baggage_price,
                'baggage'   => $baggage_price
            ];
        }
    }
}
