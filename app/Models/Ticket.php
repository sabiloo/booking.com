<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $fillable = [
        'travel_id',
        'first_name',
        'last_name',
        'gender',
        'birthdate',
        'age',
        'is_insurance'
    ];

    public function travel(){
        return $this->belongsTo(Travel::class);
    }

    public function baggage()
    {
        return $this->belongsToMany(Baggage::class, 'baggage_tickets', 'ticket_id', 'baggage_id');
    }
}
