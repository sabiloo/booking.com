<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    use HasFactory;

    public function airline(){
        return $this->belongsTo(Airline::class);
    }

    public function airport_origin(){
        return $this->belongsTo(Airport::class, 'origin_airport_id', 'id');
    }

    public function airport_destination(){
        return $this->belongsTo(Airport::class, 'destination_airport_id', 'id');
    }

    public function travels(){
        return $this->hasMany(Travel::class);
    }

    public function calculate_price($count)
    {
        return [
            'fair' => $this->price * $count,
            'taxes' => $this->taxes * $count,
            'total' => $this->price * $count + $this->taxes * $count,
            'baggage' => 0
        ];
    }
}
