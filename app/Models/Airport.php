<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    use HasFactory;

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function origins(){
        return $this->hasMany(Flight::class, 'origin_airport_id','id');
    }

    public function destinations(){
        return $this->hasMany(Flight::class, 'destination_airport_id','id');
    }
}
