# Manual Installation


## Table of content

- [Manual Installation](#Manual-Installation)
    - [Table of content](#table-of-content)
    - [Installation](#installation)

## Installation

First, clone the project

```bash
git clone https://gitlab.com/sabiloo/booking.com.git
```

After cloning the project, enter the created folder

```bash
cd booking.com
```

Now you need to install dependencies and libraries.

To install php dependencies.

```bash
composer install
```

To install npm dependencies.

```bash
npm install
```

Now copy file .env.example and name it .env

```bash
cp .env.emaple .env
```

open file and change env variables.

for generate APP_KEY run this
```bash
php artisan key:generate
```

for run migrations and seed seeders

```bahs
php artisan migrate:fresh --seed
```

default seeded user is:
> **name**: admin
>
> **email**: admin@admin.com
>
> **password**: password

now for run project enter these commands

for php server

```bash
php artisan serve
```

for run vite server

```bash
npm run dev
```
