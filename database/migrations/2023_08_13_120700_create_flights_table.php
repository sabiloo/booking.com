<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('airline_id');
            $table->unsignedBigInteger('origin_airport_id');
            $table->unsignedBigInteger('destination_airport_id');
            $table->integer('number_of_seats');
            $table->dateTime('departure_time');
            $table->dateTime('arrival_time');
            $table->enum('cabin_type',['economy','business', 'first_class']);
            $table->string('price');
            $table->boolean('is_insurance');
            $table->string('taxes');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('airline_id')->references('id')->on('airlines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('origin_airport_id')->references('id')->on('airports')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('destination_airport_id')->references('id')->on('airports')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('flights');
    }
};
