<?php

namespace Database\Factories;

use App\Models\Travel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ticket>
 */
class TicketFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender = fake()->randomElement(['male', 'female']);

        $birthdate = fake()->dateTimeBetween(Carbon::now()->timezone('Asia/Tehran')->subYear(80), Carbon::now()->timezone('Asia/Tehran'));

        $age = Carbon::now()->diffInYears(Carbon::make($birthdate)) > 13 ? 'adult' : 'children';

        return [
            'travel_id' => Travel::query()->inRandomOrder()->first()->id,
            'first_name' => fake()->name($gender),
            'last_name' => fake()->lastName(),
            'gender' => $gender,
            'birthdate' => $birthdate,
            'age' => $age,
            'is_insurance' => fake()->boolean(),
            'status' => fake()->randomDigit()

        ];
    }
}
