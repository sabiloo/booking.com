<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Traveller>
 */
class TravellerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender = fake()->randomElement(['male', 'female']);

        $birthdate = fake()->dateTimeBetween(Carbon::now()->timezone('Asia/Tehran')->subYear(80), Carbon::now()->timezone('Asia/Tehran'));

        $age = Carbon::now()->diffInYears(Carbon::make($birthdate)) > 13 ? 'adult' : 'children';

        return [
            'user_id' => User::query()->inRandomOrder()->first()->id,
            'gender' => fake()->randomElement(['male', 'female']),
            'first_name' => fake()->name($gender),
            'last_name' => fake()->lastName(),
            'birthdate' => $birthdate,
            'age' => $age
        ];
    }
}
