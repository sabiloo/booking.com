<?php

namespace Database\Factories;

use App\Models\Airline;
use App\Models\Airport;
use App\Models\City;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Flight>
 */
class FlightFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $numberOfSeats = rand(1, 200);
        $availableSeats = rand(1, $numberOfSeats);
        $departureTime = fake()->dateTimeBetween(Carbon::now()->timezone('Asia/Tehran'), Carbon::now()->timezone('Asia/Tehran')->addMonth());

        $cities = City::all()->take(10)->pluck('id')->toArray();

        return [
            'airline_id' => Airline::query()->inRandomOrder()->first()->id,
            'origin_airport_id' => Airport::query()->whereIn('city_id', $cities)->whereIn('city_id', $cities)->inRandomOrder()->first()->id,
            'destination_airport_id' => Airport::query()->whereIn('city_id', $cities)->whereIn('city_id', $cities)->inRandomOrder()->first()->id,
            'number_of_seats' => $numberOfSeats,
            'available_seats' => $availableSeats,
            'departure_time' => $departureTime,
            'arrival_time' => fake()->dateTimeBetween(Carbon::make($departureTime)->addHours(3), Carbon::make($departureTime)->addHour(3)->addMonth()),
            'cabin_type' => fake()->randomElement(['economy','business', 'first_class']),
            'price' => fake()->randomNumber(),
            'is_insurance' => fake()->boolean(),
            'taxes' => fake()->randomNumber()
        ];
    }
}
