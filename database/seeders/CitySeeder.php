<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->data() as $key => $value) {
            $country = Country::where('name', $key)->first();

            // @TODO: این روش ریختن دیتای اولیه زیاد طول می‌کشد. باید به روش bulk داده‌ها را به جدول مربوطه اضافه کرد
            if ($country) {
                foreach ($value as $city) {
                    City::query()->create([
                        'name' => $city,
                        'country_id' => $country->id
                    ]);
                }
            }
        }
    }

    private function data() : array
    {
        $cities = File::get('data/countries_and_cities.json');

        return json_decode($cities, true);
    }
}
