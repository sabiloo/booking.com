<?php

namespace Database\Seeders;

use App\Models\Airline;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class AirlineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->data() as $airline) {
            Airline::query()->create([
                'name' => $airline['name'],
                'icao' => $airline['icao'],
                'iata' => $airline['iata']
            ]);
        }
    }

    private function data(): array
    {
        $airlines = File::get('data/airlines.json');

        return json_decode($airlines, true);
    }
}
