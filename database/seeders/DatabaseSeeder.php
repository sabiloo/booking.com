<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            CountrySeeder::class,
            CitySeeder::class,
            AirportSeeder::class,
            AirlineSeeder::class,
            FlightSeeder::class,
            TravellerSeeder::class,
            TravelSeeder::class,
            TicketSeeder::class,
        ]);
    }
}
