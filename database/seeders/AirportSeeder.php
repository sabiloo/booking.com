<?php

namespace Database\Seeders;

use App\Models\Airport;
use App\Models\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class AirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->data() as $airport) {
            $city = City::where('name', $airport['city'])->first();

            // @TODO: این روش ریختن دیتای اولیه زیاد طول می‌کشد. باید به روش bulk داده‌ها را به جدول مربوطه اضافه کرد
            if ($city) {
                Airport::query()->create([
                    'name' => $airport['name'],
                    'iata_airport_code' => $airport['iata_code'],
                    'city_id' => $city->id
                ]);
            }
        }

    }

    private function data(): array
    {
        $airports = File::get('data/airports.json');

        return json_decode($airports, true);
    }
}
