<?php

namespace Database\Seeders;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $admin = User::query()->create([
            'first_name' => 'Nikola',
            'last_name' => 'Tesla',
            'birthdate' => '1856-07-10',
            'phone' => '09134567890',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);

        $admin->assignRole(Role::ADMIN->value);
        $admin->assignRole(Role::CLIENT->value);

        User::factory()->count(20)->create()->each(function ($user) {
            $user->assignRole(Role::CLIENT->value);
        });

    }
}
