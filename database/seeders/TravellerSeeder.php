<?php

namespace Database\Seeders;

use App\Models\Traveller;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TravellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Traveller::factory()->count(20)->create();
    }
}
