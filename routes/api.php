<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('cities', [\App\Http\Controllers\API\CityController::class, 'index']);

Route::get('flights', [\App\Http\Controllers\API\FlightController::class,'index']);

Route::post('/search', function () {
    $data[] = [
        'airline' => ['name' => 'test', 'logo_path' => 'test'],
        'origin_airport' => request()['origin'],
        'destination_airport' => request()['destination'],
        'date' => request()['date'],
        'price' => rand(100000,1000000),
        'seat_count' => rand(1,100),
        'time' => date('H:i'),
    ];

    $data[] = [
        'airline' => ['name' => 'test', 'logo_path' => 'test'],
        'origin_airport' => request()['origin'],
        'destination_airport' => request()['destination'],
        'date' => request()['date'],
        'price' => rand(100000,1000000),
        'seat_count' => rand(1,100),
        'time' => date('H:i'),
    ];

    return response()->json($data);
});
