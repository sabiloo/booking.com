<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('register',
    \App\Livewire\Pages\Register::class)
    ->middleware('guest')
    ->name('register');

Route::get('/',
    \App\Livewire\Pages\Index::class)
    ->name('index');

// ------------- Ganjizade ------------- //

Route::get('/traveller',
    \App\Livewire\Pages\Flight\Traveller::class)
    ->middleware('auth')
    ->name('traveller');

Route::get('/checkout',
    \App\Livewire\Pages\Flight\Checkout::class)
    ->middleware('auth')
    ->name('checkout');

Route::get('/hotel/{id}/{slug}',
    \App\Livewire\Pages\Hotel\Reservation::class)
    ->name('hotel.show');

Route::get('/hotel/book',
    \App\Livewire\Pages\Hotel\Book::class)
    ->name('hotel.book');

// ------------- Ganjizade ------------- //

Route::name('hotel.')
    ->group(function () {
        Route::get('hotels',
            \App\Livewire\HotelSearch::class)
            ->name('index');
    });

